<?php
/**
 * «O-RCON.WHILETEAM.RU», © 2018
 * Author: KobaltMR
 */

define('INSTALLATION_SC', true);

require dirname(__FILE__) . '/../app/init.php';

if ($core->getConfig()->installed === true) {
    sleep(1);
    header("Location: /auth");
    exit;
}

$message = [
    'type' => '',
    'text' => '',
];

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $installationState = install();
    if ($installationState == true) {
        sleep(1);
        header("Location: /auth");
        exit;
    } else if (is_array($installationState) && count($installationState) == 2) {
        $message['type'] = $installationState[0];
        $message['text'] = $installationState[1];
    } else {
        $message['type'] = 'error';
        $message['text'] = 'Неизвестная ошибка!';
    }
}

echo $core->renderTpl('/install/main.twig', [
    'message' => $message
]);

function install()
{
    global $core;

    $siteName = filter_input(INPUT_POST, 'siteName', FILTER_DEFAULT);
    $db_host = filter_input(INPUT_POST, 'db_host', FILTER_DEFAULT);
    $db_port = filter_input(INPUT_POST, 'db_port', FILTER_SANITIZE_NUMBER_INT);
    $db_user = filter_input(INPUT_POST, 'db_user', FILTER_DEFAULT);
    $db_pass = filter_input(INPUT_POST, 'db_pass', FILTER_DEFAULT);
    $db_name = filter_input(INPUT_POST, 'db_name', FILTER_DEFAULT);

    $core->getConfig()->installed = true;
    $core->getConfig()->global->siteName = $siteName;
    $core->getConfig()->db->host = $db_host;
    $core->getConfig()->db->port = $db_port;
    $core->getConfig()->db->user = $db_user;
    $core->getConfig()->db->pass = $db_pass;
    $core->getConfig()->db->name = $db_name;
    $core->getConfig()->debug = (isset($_POST['debug']) && $_POST['debug'] == 'on') ? true : false;

    if (empty($siteName) || empty($db_host) || empty($db_user) || empty($db_name) || empty($db_port)) {
        return ['error', 'Недостаточно данных!'];
    }

    try {
        $connection = new \PDO(
            'mysql:host=' . $db_host .
            ';dbname=' . $db_name .
            ';port=' . $db_port .
            ';charset=utf8',
            $db_user,
            $db_pass,
            [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            ]
        );
    } catch (Exception $ex) {
        return ['error', 'Не удалось подключиться к базе данных!<br>' . $ex->getMessage()];
    }

    try {
        $restore = $connection->prepare(file_get_contents('../db.sql.install'));
        $restore->execute();
    } catch (Exception $ex) {
        return ['error', 'Не удалось выполнить запрос!<br>' . $ex->getMessage()];
    }

    try {
        $core->getConfig()->__save();
    } catch (Exception $ex) {
        return ['error', 'Не удалось сохранить конфигурацию!<br>' . $ex->getMessage()];
    }

    return true;
}