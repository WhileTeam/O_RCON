$(function () {
    $('#authForm').on('submit', function (event) {
        event.preventDefault();
        var form = $(this);

        $.ajax({
            url: '/auth',
            method: "post",
            data: form.serializeArray(),
            beforeSend: function () {
                form.find('button[type="submit"]').attr('disabled', 'disabled');
            }
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data['type'] === undefined || data['type'] === null ||
                    data['message'] === undefined || data['message'] === null) throw '';
            } catch (ex) {
                console.log(data);
                return alert('error', 'Не удалось выполнить запрос<br>(невалидный ответ)');
            }

            if (data['type'] === 'success') {
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            } else {
                setTimeout(function () {
                    form.find('button[type="submit"]').removeAttr('disabled');
                }, 3000);
            }

            alert(data['type'], data['message']);
        }).fail(function (data) {
            form.find('button[type="submit"]').removeAttr('disabled');
            alert('error', 'Не удалось выполнить запрос<br>(невалидный ответ)');
            console.log(data);
        });
    });
});

function alert(type, message) {
    var className;
    var alertContainer = $('#alertContainer');
    if (message === undefined || message === null || message === '' ||
        type === undefined || type === null || type === '') return false;

    switch (type) {
        case 'error':
        default:
            className = 'alert-danger';
            break;

        case 'warning':
            className = 'alert-warning';
            break;

        case 'success':
            className = 'alert-success';
            break;
    }

    alertContainer.fadeOut('300');
    setTimeout(function () {
        alertContainer.attr('class', 'text-center alert ' + className).html(message).fadeIn(300);
    }, 300);

    return true;
}