var eID = false;
var caErr = 0;
var cDaemon = false;
var checkerTime = 1000;
var commandHistory = [];
var hc = false;

$(function () {
    var consoleContainer = $('.console');

    consoleContainer.removeClass('c-wait');
    consoleContainer.find('[disabled]').removeAttr('disabled');
    consoleContainer.find('input').focus();
    addLog('Консоль готова к работе!', false, 'Консоль готова к работе и ожидает команду!', 'text-warning');

    //for (var i = 1; i <= 20; i++) {setTimeout(function () {var classD = '';switch (Math.floor((Math.random() * 6) + 1)) {case 2:classD = 'text-info';break;case 3:classD = 'text-warning';break;case 4:classD = 'text-primary';break;case 5:classD = 'text-danger';break;case 6:classD = 'text-success';break;}addLog('Консоль готова к работе!', false, false, classD);}, 1100 * i);}

    consoleContainer.find('input').on('keydown', function (event) {
        if (consoleContainer.find('input').attr('disabled') === undefined && (event.keyCode === 38 || event.keyCode === 40) && commandHistory.length > 0) {
            event.preventDefault();
            if (event.keyCode === 38) {
                if (hc !== false) {
                    if (typeof hc !== "number" || hc >= (commandHistory.length - 1)) hc = 0;
                    else hc++;
                } else hc = 0;
            } else {
                if (hc !== false) {
                    if (typeof hc !== "number" || hc <= 0) hc = (commandHistory.length - 1);
                    else hc--;
                } else hc = (commandHistory.length - 1);
            }
            consoleContainer.find('input').val(commandHistory[(hc)]);
            return false;
        } else {
            hc = false;
        }

        return true;
    });

    consoleContainer.find('form').on('submit', function (event) {
        event.preventDefault();
        var form = $(this);

        $.ajax({
            url: form.attr('action'),
            method: "post",
            data: form.serializeArray(),
            beforeSend: function () {
                form.find('input').attr('disabled', 'disabled');
                form.find('button[type="submit"]').attr('disabled', 'disabled');
            }
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data['type'] === undefined || data['type'] === null ||
                    data['message'] === undefined || data['message'] === null) throw '';
            } catch (ex) {
                console.log(data);
                return addLog('Не удалось выполнить запрос (невалидный ответ)', false, false, 'text-danger');
            }

            if (data['type'] === 'error') {
                addLog(data['message'], false, false, 'text-danger');

                if (data['data'] !== undefined && data['data'].length > 0) {
                    if (data['data']['command'] !== undefined && parseInt(data['data']['command']['id']) !== eID) {
                        addLog(data['data']['command']['command'], (data['data']['command']['creation_time'] * 1000), data['message'], 'text-info');
                        form.find('input').val(data['data']['command']['command']);
                    }
                } else {
                    setTimeout(function () {
                        form.find('input').removeAttr('disabled');
                        form.find('button[type="submit"]').removeAttr('disabled');
                    }, 3000);
                }
            } else if (data['type'] === 'success') {
                addLog(data['data']['command'], false, data['message'], 'text-primary');
                eID = parseInt(data['data']['eID']);
            }
        }).fail(function (data) {
            form.find('input').removeAttr('disabled');
            form.find('button[type="submit"]').removeAttr('disabled');
            console.log('Не удалось выполнить запрос (невалидный ответ)');
            console.log(data);
        });
    });

    initDaemon(checkerTime);
});

function addLog(text, time, tooltipText, cssClass) {
    var console = $('.console ul');
    $('<li />', {
        class: ((cssClass !== false && cssClass !== undefined) ? cssClass : ''),
        html: "<span class='time'>[" + hTime(((time !== false && time !== undefined) ? time : false)) + "]</span> "
        + text,
        'data-toggle': 'tooltip',
        'title': ((tooltipText !== false && tooltipText !== undefined) ? tooltipText : '')
    }).appendTo(console).hide().slideDown(300);

    var scrollHeight = 0;
    console.find('li').each(function (i, e) {
        if (typeof $(e).height() === 'number') scrollHeight += $(e).outerHeight();
    });
    console.animate({scrollTop: scrollHeight}, "slow");

    initStyles();
}

function initDaemon(intervalTime) {
    if (typeof intervalTime !== "number" || intervalTime < 1000 || intervalTime > 30000) intervalTime = 1000;

    if (cDaemon !== false) clearInterval(cDaemon);

    var form = $('.console').find('form');
    cDaemon = setInterval(function () {
        if (caErr > 2 || typeof caErr !== "number") {
            eID = false;
            caErr = 0;
            initDaemon(1000);
        }

        if (eID === false || typeof eID !== "number") {
            /*form.find('input').removeAttr('disabled');
            form.find('button[type="submit"]').removeAttr('disabled');*/
            return false;
        }
        clearInterval(cDaemon);

        $.ajax({
            url: '/executeStatus/' + eID,
            method: "post"
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data['type'] === undefined || data['type'] === null ||
                    data['message'] === undefined || data['message'] === null) throw '';
            } catch (ex) {
                console.log(data);
                if (typeof caErr !== "number") caErr = 3;
                else caErr++;
                initDaemon(intervalTime + 2000);
                return console.log('Не удалось выполнить запрос (невалидный ответ)');
            }

            if (data['type'] === 'error') {
                eID = false;
                addLog(data['message'], false, false, 'text-danger');
            } else if (data['type'] === 'success') {
                if (data['data']['repeat'] !== true) {
                    eID = false;
                    addLog(data['data']['response'], false, data['message'], false);

                    commandHistory = [form.find('input').val()].concat(commandHistory);
                    form.find('input').removeAttr('disabled').val('').focus();
                    form.find('button[type="submit"]').removeAttr('disabled');
                }
            }

            initDaemon(checkerTime);
        }).fail(function (data) {
            console.log('Не удалось выполнить запрос (невалидный ответ)');
            console.log(data);
            if (typeof caErr !== "number") caErr = 3;
            else caErr++;
            initDaemon(intervalTime + 2000);
        });
    }, intervalTime);
}

function hTime(time) {
    var date;

    if (time !== undefined && time !== null && time !== false && typeof time === "number") date = new Date(time);
    else date = new Date();

    return ((date.getHours() < 10) ? "0".toString() + date.getHours() : date.getHours()) + ':' + (
        (date.getMinutes() < 10) ? "0".toString() + date.getMinutes() : date.getMinutes()) + ':' + (
        (date.getSeconds() < 10) ? "0".toString() + date.getSeconds() : date.getSeconds());
}

function initStyles() {
    $('[data-toggle="tooltip"]').tooltip();
}
