$(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('form[data-ajax]').on('submit', function (event) {
        event.preventDefault();
        var form = $(this);

        if (form.attr('data-confirm') !== undefined) if (!confirm(form.attr('data-confirm'))) return;

        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: form.serializeArray(),
            beforeSend: function () {
                form.find('input:not([dontChangeState]), select, checkbox, button').attr('disabled', 'disabled');
            }
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data['type'] === undefined || data['type'] === null ||
                    data['message'] === undefined || data['message'] === null) throw '';
            } catch (ex) {
                form.find('input:not([dontChangeState]), select, checkbox, button').removeAttr('disabled');
                console.log(data);
                alert('error', 'Невалидный ответ!');
                return false;
            }

            if (data['type'] === 'error') {
                form.find('input:not([dontChangeState]), select, checkbox, button').removeAttr('disabled');
            } else {
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            }

            $('body, html').animate({
                scrollTop: 0
            }, 300);
            alert(data['type'], data['message']);
        }).fail(function (data) {
            form.find('input:not([dontChangeState]), select, checkbox, button').removeAttr('disabled');
            console.log(data);
            alert('error', 'Невалидный ответ!');
        });
    });
});

function addItem(container, appendToContainer, id) {
    var nId = $(appendToContainer).find('[data-id]:last-child').attr('data-id');
    if (nId !== undefined && nId !== null) id = (parseInt(nId) + 1);
    else id = parseInt(id) + 1;

    $($(container).clone().html(function (i, html) {
        return html.replace(/\$id/g, id);
    }).html()).appendTo(appendToContainer);
    $('[data-toggle="tooltip"]').tooltip();

    $(appendToContainer).find('[data-list-empty]').remove();
}

function removeItem(container, id) {
    $(container).find('[data-id="' + id + '"]').remove();
    $('.tooltip').remove();
}

function alert(type, message) {
    var className;
    var alertContainer = $('#alertContainer');
    if (message === undefined || message === null || message === '' ||
        type === undefined || type === null || type === '') return false;

    switch (type) {
        case 'error':
        default:
            className = 'alert-danger';
            break;

        case 'warning':
            className = 'alert-warning';
            break;

        case 'success':
            className = 'alert-success';
            break;
    }

    alertContainer.fadeOut('300');
    setTimeout(function () {
        alertContainer.attr('class', 'text-center alert ' + className).html(message).fadeIn(300);
    }, 300);

    return true;
}