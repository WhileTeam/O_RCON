<?php
/**
 * «O-RCON.WHILETEAM.RU», © 2018
 * Author: KobaltMR
 */

require dirname(__FILE__) . '/../app/init.php';

if ($core->checkInstallation()) echo $core->setupController();
else if (!defined('INSTALLATION_SC')) header("Location: /install.php");