<?php
return array (
  'installed' => false,
  'debug' => false,
  'global' => 
  array (
    'templateCache' => false,
    'siteName' => 'My Site',
  ),
  'db' => 
  array (
    'host' => '127.0.0.1',
    'port' => '3306',
    'user' => '',
    'pass' => '',
    'name' => '',
  ),
  'rcon' => 
  array (
    'executor' => '\\O_RCON\\App\\Executors\\DaemonExecutor',
    'blockedPrefixes' => 
    array (),
  ),
  'routes' => 
  array (
    0 => 
    array (
      'method' => 'get',
      'uri' => '/',
      'controller' => 'DashboardController@index',
    ),
    1 => 
    array (
      'method' => 'get',
      'uri' => '/server/(int)',
      'controller' => 'DashboardController@index',
    ),
    2 => 
    array (
      'method' => 'post',
      'uri' => '/sendCommand/(int)',
      'controller' => 'DashboardController@sendCommand',
    ),
    3 => 
    array (
      'method' => 'post',
      'uri' => '/executeStatus/(int)',
      'controller' => 'DashboardController@executeStatus',
    ),
    4 => 
    array (
      'method' => 'get',
      'uri' => '/settings',
      'controller' => 'AdminController@stats',
    ),
    5 => 
    array (
      'method' => 'get',
      'uri' => '/settings/main',
      'controller' => 'AdminController@mainSettings',
    ),
    6 => 
    array (
      'method' => 'post',
      'uri' => '/settings/main',
      'controller' => 'AdminController@mainSettingsPost',
    ),
    7 => 
    array (
      'method' => 'get',
      'uri' => '/settings/servers',
      'controller' => 'AdminController@serversSettings',
    ),
    8 => 
    array (
      'method' => 'post',
      'uri' => '/settings/servers',
      'controller' => 'AdminController@serversSettingsPost',
    ),
    9 => 
    array (
      'method' => 'get',
      'uri' => '/settings/limits',
      'controller' => 'AdminController@limitsSettings',
    ),
    10 => 
    array (
      'method' => 'post',
      'uri' => '/settings/limits',
      'controller' => 'AdminController@limitsSettingsPost',
    ),
    11 => 
    array (
      'method' => 'get',
      'uri' => '/settings/users',
      'controller' => 'AUsersController@users',
    ),
    12 => 
    array (
      'method' => 'get',
      'uri' => '/settings/users/add',
      'controller' => 'AUsersController@addUser',
    ),
    13 => 
    array (
      'method' => 'post',
      'uri' => '/settings/users/add',
      'controller' => 'AUsersController@addUserPost',
    ),
    14 => 
    array (
      'method' => 'get',
      'uri' => '/settings/users/edit/(int)',
      'controller' => 'AUsersController@editUser',
    ),
    15 => 
    array (
      'method' => 'post',
      'uri' => '/settings/users/edit/(int)',
      'controller' => 'AUsersController@editUserPost',
    ),
    16 => 
    array (
      'method' => 'post',
      'uri' => '/settings/users/delete/(int)',
      'controller' => 'AUsersController@deleteUserPost',
    ),
    17 => 
    array (
      'method' => 'get',
      'uri' => '/settings/q/clearQueue',
      'controller' => 'AdminController@clearQueue',
    ),
    18 => 
    array (
      'method' => 'get',
      'uri' => '/settings/q/stopDaemons',
      'controller' => 'AdminController@stopDaemons',
    ),
    20 => 
    array (
      'method' => 'get',
      'uri' => '/auth',
      'controller' => 'AuthController@index',
    ),
    21 => 
    array (
      'method' => 'post',
      'uri' => '/auth',
      'controller' => 'AuthController@process',
    ),
    22 => 
    array (
      'method' => 'get',
      'uri' => '/logout',
      'controller' => 'AuthController@logout',
    ),
  ),
);