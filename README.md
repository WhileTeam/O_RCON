# O-RCON
###### Веб RCON-консоль

[Скриншоты](https://gitlab.com/WhileTeam/O_RCON/wikis/Screenshots) | [Демо сайт](https://o-rcon.whileteam.ru/)

### Зависимости:
- PHP >= 7.0
- [Twig](https://twig.symfony.com/)
- [Config-Manager](https://gitlab.com/KobaltMR/Config-Manager)
- [EasyRouter](https://github.com/nickstuer/EasyRouter)
- [PHP-Minecraft-Rcon](https://github.com/thedudeguy/PHP-Minecraft-Rcon) 

Для работы скрипта требуется включить модуль rewrite для apache.

### Способы работы с RCON:
1) При каждом запросе на выполнении команды инициализируется новое подключение к RCON, после выполнения команды подключение завершается
2) **(Рекомендуется)** Подключение к каждом серверу запускается в отличных фоновых процессах. Каждый процесс проверяет наличие команд в очереди на выполнение, после выполнения команды фоновое подключение не завершается **(требуется включённый модуль php-posix)**

Пример команды для запуска подключений к RCON:

`php -t /web/o-rcon.local/public_html/public/ -f /web/o-rcon.local/public_html/app/initDaemon.php`

### Установка:
1. Загрузите репозиторий
2. Установите директиву `DOCUMENT_ROOT` на папку `/public`
3. Для запуска процесса установки перейдите по адресу: `domain.ru`/install.php

После успешной установки будет создан пользователь со следующими данными: `admin@teste.ru`:`admin`

### Тех. Поддержка:
Техническая поддержка осуществляеться на форуме RUMINE.ORG: [Ссылка на тему с тех. добровольной поддержкой](https://rumine.org/threads/148/)

Для **покупателей** скрипта [LDonate](https://ldonate.ru) осуществляеться тех. поддержка от авторов по следующим контактам:
- Website: whileteam.ru
- VK: [WhileTeam](https://vk.me/whileteam)