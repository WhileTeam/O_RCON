<?php
/**
 * «O-RCON.WHILETEAM», © 2018
 * Author: KobaltMR
 */

namespace O_RCON\Controller;

use O_RCON\App\BasicController;
use O_RCON\App\DB;
use O_RCON\App\User;

class AuthController extends BasicController
{

    public function index()
    {
        if (User::$is_auth === true) exit(header('Location: /'));

        echo $this->core->renderTpl('controllers/auth/form.twig');
    }

    public function process()
    {
        if (User::$is_auth === true) exit(header('Location: /'));

        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $passw = filter_input(INPUT_POST, 'password');

        if (empty($email) || empty($passw)) return $this->jsonResponse('error', 'Введите эл. почту и пароль!');
        $db = DB::connect();

        $checkCredentials = $db->prepare("SELECT `uid`, `password` FROM `or_user` WHERE `email` = ? LIMIT 1");
        if (!$checkCredentials->execute([$email])) {
            return $this->jsonResponse('error', 'Не удалось выполнить запрос!<br>Попробуйте повторить попытку позже');
        }
        if ($checkCredentials->rowCount() < 1) {
            return $this->jsonResponse('error', 'Пользователь не найден!');
        }

        $user = $checkCredentials->fetch();
        if (!password_verify($passw, $user['password'])) {
            return $this->jsonResponse('error', 'Неверный пароль!');
        }

        User::setAuthSession($email, $passw);
        return $this->jsonResponse('success', 'Вы успешно авторизованы!');
    }

    public function logout()
    {
        $this->core->user->logout();

        exit(header('Location: /auth'));
    }

    private function jsonResponse($type, $message)
    {
        exit(json_encode([
            'type' => $type,
            'message' => $message,
        ], JSON_UNESCAPED_UNICODE));
    }
}