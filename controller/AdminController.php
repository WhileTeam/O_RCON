<?php
/**
 * «O-RCON.WHILETEAM», © 2018
 * Author: KobaltMR
 */

namespace O_RCON\Controller;


use O_RCON\App\BasicController;
use O_RCON\App\Core;
use O_RCON\App\DB;

class AdminController extends BasicController
{
    public function __construct()
    {
        parent::__construct();
        if ($this->core->user->is_admin !== true) exit(header('Location: /auth'));
    }

    public function stats()
    {
        $db = DB::connect();
        $userQuery = $db->query("SELECT
                COUNT(`or_user`.`uid`) AS `totalUsers`, 
                SUM(CASE WHEN `or_user`.`first_seen` > " . strtotime('-1 day') . " THEN 1 ELSE 0 END) AS `todayRegUsers`,
                SUM(CASE WHEN `or_user`.`last_seen` > " . strtotime('-1 day') . " THEN 1 ELSE 0 END) AS `activeDayUsers`,
                SUM(CASE WHEN `or_user`.`last_seen` > " . strtotime('-5 minutes') . " THEN 1 ELSE 0 END) AS `onlineNowUsers`
            FROM `or_user`");
        $commandsQuery = $db->query("SELECT
                COUNT(`or_query`.`id`) AS `totalCommands`,
                SUM(CASE WHEN `or_query`.`creation_time` > " . strtotime('-1 day') . " THEN 1 ELSE 0 END) AS `createdDayCommands`,
                SUM(CASE WHEN `or_query`.`execution_time` IS NULL THEN 1 ELSE 0 END) AS `notExecutedCommands`
            FROM `or_query`");
        $serverPids = $db->query("SELECT `title`, `pid` FROM `or_server` ORDER BY `id`");

        $userQuery = $userQuery->fetch();
        $commandsQuery = $commandsQuery->fetch();
        $serverPids = $serverPids->fetchAll();

        $stats = [
            'users' => [
                'total' => [
                    'title' => 'Пользователей всего:',
                    'value' => (int) $userQuery['totalUsers'],
                ],
                'created_today' => [
                    'title' => 'Пользователей созданных сегодня:',
                    'value' => (int) $userQuery['todayRegUsers'],
                ],
                'active_today' => [
                    'title' => 'Пользователей проявивших активность сегодня:',
                    'value' => (int) $userQuery['activeDayUsers'],
                ],
                'online_now' => [
                    'title' => 'Пользователей онлайн сейчас:',
                    'value' => (int) $userQuery['onlineNowUsers'],
                ],
            ],
            'query' => [
                'total' => [
                    'title' => 'Всего команд в очереди:',
                    'value' => (int) $commandsQuery['totalCommands'],
                ],
                'created_today' => [
                    'title' => 'Команд добавленных сегодня:',
                    'value' => (int) $commandsQuery['createdDayCommands'],
                ],
                'notExecuted' => [
                    'title' => 'В очереди на выполнение:',
                    'value' => (int) $commandsQuery['notExecutedCommands'],
                ],
            ],
        ];

        foreach ($serverPids as &$serverPid) {
            if (!empty($serverPid['pid']) && posix_getpgid($serverPid['pid']) == true) $serverPid['status'] = 1;
            else $serverPid['status'] = 0;
        }

        echo $this->core->renderTpl('controllers/admin/stats.twig', [
            'stats' => $stats,
            'pids' => $serverPids
        ]);
    }

    public function mainSettings()
    {
        echo $this->core->renderTpl('controllers/admin/mainSettings.twig');
    }

    public function serversSettings()
    {
        $db = DB::connect();
        $servers = $db->query("SELECT * FROM `or_server` ORDER BY `id`");
        echo $this->core->renderTpl('controllers/admin/serversSettings.twig', [
            'servers' => $servers,
        ]);
    }

    public function limitsSettings()
    {
        $commands = $this->config->rcon->blockedPrefixes->__toArray();
        $blockType = ((in_array('*', $this->config->rcon->blockedPrefixes->__toArray())) ? 2 : 1);

        echo $this->core->renderTpl('controllers/admin/limitsSettings.twig', [
            'commands' => $commands,
            'blockType' => $blockType
        ]);
    }

    public function usersSettings()
    {
        echo $this->core->renderTpl('controllers/admin/usersSettings.twig');
    }

    public function clearQueue()
    {
        $db = DB::connect();
        $db->query("TRUNCATE TABLE `or_query`");
        exit(header('Location: /settings'));
    }

    public function stopDaemons()
    {
        $db = DB::connect();
        $db->query("UPDATE `or_server` SET `pid` = NULL");
        exit(header('Location: /settings'));
    }

    public function mainSettingsPost()
    {
        if (!filter_var($_POST['dbHost'], FILTER_VALIDATE_IP)) return $this->jsonResponse('error', 'Невалидный IP! [' . $_POST['dbHost'] . ']');
        if (!filter_var($_POST['dbPort'], FILTER_VALIDATE_INT, ['options' => ['min_range' => 1024, 'max_range' => 65535]])) return $this->jsonResponse('error', 'Невалидный порт! [' . $_POST['dbPort'] . ']');

        $this->core->getConfig()->global->siteName = filter_input(INPUT_POST, 'siteName', FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES);
        $this->core->getConfig()->global->templateCache = (isset($_POST['templateCache']) && $_POST['templateCache'] == 'on') ? true : false;
        $this->core->getConfig()->db->host = $_POST['dbHost'];
        $this->core->getConfig()->db->port = $_POST['dbPort'];
        $this->core->getConfig()->db->user = filter_input(INPUT_POST, 'dbUser', FILTER_SANITIZE_MAGIC_QUOTES);
        $this->core->getConfig()->db->name = filter_input(INPUT_POST, 'dbName', FILTER_SANITIZE_MAGIC_QUOTES);

        if (isset($_POST['changeDBPass']) && $_POST['changeDBPass'] == 'on') {
            $this->core->getConfig()->db->pass = filter_input(INPUT_POST, 'dbPass', FILTER_SANITIZE_MAGIC_QUOTES);
        }

        $executor = filter_input(INPUT_POST, 'rconExecutorType', FILTER_SANITIZE_STRING);

        if (class_exists($executor)) {
            if ($this->core->getConfig()->rcon->executor !== $executor) {
                $db = DB::connect();
                $db->query("UPDATE `or_server` SET `pid` = NULL");
            }
            $this->core->getConfig()->rcon->executor = $executor;
        } else {
            return $this->jsonResponse('error', 'Неизвестный тип работы с RCON! [класс не найден | ' . $executor . ']');
        }

        $this->core->getConfig()->debug = (isset($_POST['debug']) && $_POST['debug'] == 'on') ? true : false;

        try {
            $this->core->getConfig()->__save();
        } catch (\Exception $exception) {
            return $this->jsonResponse('error', 'Не удалось сохранить настройки!' . PHP_EOL . $exception->getMessage());
        }
        return $this->jsonResponse('success', 'Настройки успешно изменены!');
    }

    public function serversSettingsPost()
    {
        if (empty($_POST['servers']) || !is_array($_POST['servers'])) return $this->jsonResponse('error', 'Недостаточно данных!');

        $db = DB::connect();
        $servers = $_POST['servers'];

        foreach ($servers as $sID => $server) {
            if (!Core::checkArrayFields(['title', 'ip', 'port', 'pass'], $server)) {
                return $this->jsonResponse('error', 'Недостаточно данных!');
            }

            $server['title'] = htmlspecialchars($server['title']);
            if (!filter_var($server['ip'], FILTER_VALIDATE_IP)) return $this->jsonResponse('error', 'Невалидный IP! [' . $server['ip'] . ']');
            if (!filter_var($server['port'], FILTER_VALIDATE_INT, ['options' => ['min_range' => 1024, 'max_range' => 65535]])) return $this->jsonResponse('error', 'Невалидный порт! [' . $server['port'] . ']');

            $checkServerIsset = $db->prepare("SELECT `id` FROM `or_server` WHERE `id` = ? LIMIT 1");
            if (!$checkServerIsset->execute([$sID])) {
                return $this->jsonResponse('error', 'Не удалось выполнить запрос к БД!');
            }

            if ($checkServerIsset->rowCount() > 0) {
                $insert = $db->prepare("UPDATE `or_server` SET `title` = ?, `ip` = ?, `port` = ?, `pass` = ? WHERE `id` = ? LIMIT 1");
                if (!$insert->execute([
                    $server['title'],
                    $server['ip'],
                    $server['port'],
                    $server['pass'],
                    $sID
                ])) {
                    return $this->jsonResponse('error', 'Не удалось выполнить запрос к БД!');
                }
            } else {
                $insert = $db->prepare("INSERT INTO `or_server` (`title`, `ip`, `port`, `pass`) VALUES (?, ?, ?, ?)");
                if (!$insert->execute([
                    $server['title'],
                    $server['ip'],
                    $server['port'],
                    $server['pass']
                ])) {
                    return $this->jsonResponse('error', 'Не удалось выполнить запрос к БД!');
                }
            }
        }

        return $this->jsonResponse('success', 'Настройки успешно сохранены!');
    }

    public function limitsSettingsPost()
    {
        if (empty($_POST['commands']) || !is_array($_POST['commands'])) return $this->jsonResponse('error', 'Недостаточно данных!');
        $commands = [];
        if (!empty($_POST['blockType']) && $_POST['blockType'] == 2) $commands[] = '*';

        foreach ($_POST['commands'] as $command) {
            if (!empty($command) && is_string($command) && $command != '*') $commands[] = $command;
        }

        $this->config->rcon->blockedPrefixes = $commands;
        try {
            $this->core->getConfig()->__save();
        } catch (\Exception $exception) {
            return $this->jsonResponse('error', 'Не удалось сохранить настройки!' . PHP_EOL . $exception->getMessage());
        }

        return $this->jsonResponse('success', 'Настройки успешно сохранены!');
    }

    private function jsonResponse($type, $message, $data = false)
    {
        exit(json_encode([
            'type' => $type,
            'message' => $message,
            'data' => $data
        ], JSON_UNESCAPED_UNICODE));
    }
}