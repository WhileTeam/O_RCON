<?php
/**
 * «O-RCON.WHILETEAM», © 2018
 * Author: KobaltMR
 */

namespace O_RCON\Controller;

use O_RCON\App\BasicController;
use O_RCON\App\CommandQuery;
use O_RCON\App\DB;
use O_RCON\App\User;

class DashboardController extends BasicController
{
    public function index($server = false)
    {
        if (User::$is_auth === false) exit(header('Location: /auth'));

        $db = DB::connect();
        $servers = $db->query("SELECT * FROM `or_server`");
        $servers = $servers->fetchAll();

        if (\filter_var($server, \FILTER_VALIDATE_INT, ['options' => ['min_range' => 0, 'max_range' => 1000000]])) {
            foreach ($servers as $serverItem) {
                if ($serverItem['id'] === $server) {
                    $server = $serverItem;
                }
            }

            if (!\is_array($server)) $server = false;
        } else {
            $server = false;
        }

        echo $this->core->renderTpl('controllers/dashboard/main.twig', [
            'servers' => $servers,
            'server' => $server,
        ]);
    }

    public function sendCommand($server)
    {
        if (User::$is_auth === false) $this->jsonResponse('error', 'Требуется авторизация!');

        $command = trim(filter_input(INPUT_POST, 'command', FILTER_SANITIZE_STRING));
        if (empty($command)) return $this->jsonResponse('error', 'Введите команду!');
        $server = (int)$server;

        $add = CommandQuery::addCommand($server, $command);

        if ($add['success'] === false || !isset($add['additionalData']['id'])) {
            return $this->jsonResponse('error', $add['message'], ((isset($add['additionalData'])) ? $add['additionalData'] : []));
        }

        return $this->jsonResponse('success', 'Команда успешно добавлена в очередь!', [
            'command' => $command,
            'eID' => $add['additionalData']['id'],
        ]);
    }

    public function executeStatus($id)
    {
        if (User::$is_auth === false) $this->jsonResponse('error', 'Требуется авторизация!');

        $id = (int)$id;

        $response = CommandQuery::getResult($id);

        if ($response['success'] === true && isset($response['additionalData']['response'])) {
            /** © <Toster_tpl> */
            $styles = [
                '§0' => '<span style="color: #000;">', '§1' => '<span style="color: #00a;">',
                '§2' => '<span style="color: #0a0;">', '§3' => '<span style="color: #0aa;">',
                '§4' => '<span style="color: #e07169;">', '§5' => '<span style="color: #a0a;">',
                '§6' => '<span style="color: #fa0;">', '§7' => '<span style="color: #aaa;">',
                '§8' => '<span style="color: #555;">', '§9' => '<span style="color: #55f;">',
                '§a' => '<span style="color: #5f5;">', '§b' => '<span style="color: #5ff;">',
                '§c' => '<span style="color: #f55;">', '§d' => '<span style="color: #f5f;">',
                '§e' => '<span style="color: #ff5;">', '§f' => '<span style="color: #fff;">',
                '§k' => '<span class="text-obfuscated">', '§l' => '<span style="font-weight: bold;">',
                '§m' => '<span style="text-decoration: line-through;">', '§n' => '<span style="text-decoration: underline;">',
                '§o' => '<span style="font-style: italic;">', '§r' => '<span style="color: #000; font-weight: normal; text-decoration: none;">',
            ];
            $response['additionalData']['response'] = str_replace(array_keys($styles), array_values($styles), $response['additionalData']['response'], $count);
            for ($i = 0; $i < $count; $i++) {
                $response['additionalData']['response'] .= '</span>';
            }
            /** © </Toster_tpl> */

            if (empty($response['additionalData']['response']) || empty(strip_tags($response['additionalData']['response']))) {
                $response['additionalData']['response'] = 'Команда успешно выполнена!';
            }

            return $this->jsonResponse('success', $response['message'], $response['additionalData']);
        } else {
            return $this->jsonResponse(($response['success'] == true ? 'success' : 'error'), $response['message'], ((isset($response['additionalData'])) ? $response['additionalData'] : []));
        }
    }

    private function jsonResponse($type, $message, $data = false)
    {
        exit(json_encode([
            'type' => $type,
            'message' => $message,
            'data' => $data
        ], JSON_UNESCAPED_UNICODE));
    }
}