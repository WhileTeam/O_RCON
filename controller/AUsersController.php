<?php
/**
 * «O-RCON.WHILETEAM», © 2018
 * Author: KobaltMR
 */

namespace O_RCON\Controller;


use O_RCON\App\BasicController;
use O_RCON\App\DB;
use O_RCON\App\User;

class AUsersController extends BasicController
{
    public function __construct()
    {
        parent::__construct();
        if ($this->core->user->is_admin !== true) exit(header('Location: /auth'));
    }

    public function users()
    {
        $db = DB::connect();

        $users = $db->query("SELECT `uid`, `email`, `first_seen`, `last_seen`, `is_admin` FROM `or_user` ORDER BY `uid`");

        echo $this->core->renderTpl('controllers/admin/users/list.twig', [
            'users' => $users->fetchAll()
        ]);
    }

    public function addUser()
    {
        echo $this->core->renderTpl('controllers/admin/users/add.twig');
    }

    public function editUser($uid)
    {
        $db = DB::connect();

        $user = $db->query("SELECT `uid`, `email`, `first_seen`, `last_seen`, `is_admin` FROM `or_user` WHERE `uid` = '{$uid}' LIMIT 1");

        echo $this->core->renderTpl('controllers/admin/users/edit.twig', [
            'user' => ($user->rowCount() >= 1) ? $user->fetch() : false
        ]);
    }

    public function addUserPost()
    {
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $password = filter_input(INPUT_POST, 'password', FILTER_DEFAULT);
        $is_admin = (isset($_POST['is_admin']) && $_POST['is_admin'] == 'on') ? 1: 0;

        if (empty($email) || empty($password)) {
            return $this->jsonResponse('error', 'Заполните все поля формы!');
        }

        $db = DB::connect();
        $checkEmail = $db->prepare("SELECT `uid` FROM `or_user` WHERE `email` = ? LIMIT 1");
        if (!$checkEmail->execute([$email])) {
            return $this->jsonResponse('error', 'Ошибка при работе с БД (email exist query)!');
        }

        if ($checkEmail->rowCount() >= 1) {
            return $this->jsonResponse('error', 'Пользователь с таким EMail уже существует!');
        }

        $insert = $db->prepare("INSERT INTO `or_user` (email, password, first_seen, last_seen, is_admin) VALUES (?, ?, ?, ?, ?)");
        if (!$insert->execute([
            $email,
            User::encryptPass($password),
            time(),
            0,
            $is_admin
        ])) {
            return $this->jsonResponse('error', 'Ошибка при работе с БД (user creation query)!');
        }

        return $this->jsonResponse('success', 'Пользователь успешно создан!');
    }

    public function editUserPost($uid)
    {
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        if (isset($_POST['changePassword']) && $_POST['changePassword'] == 'on') {
            $password = User::encryptPass(filter_input(INPUT_POST, 'password'));
        }
        $is_admin = (isset($_POST['is_admin']) && $_POST['is_admin'] == 'on') ? 1: 0;

        $db = DB::connect();

        $checkUser = $db->prepare("SELECT `or_user`.`uid`, `or_user`.`password`, `checkEmail`.`email` as `emailExist` FROM `or_user`
            LEFT JOIN `or_user` AS `checkEmail` ON `checkEmail`.`uid` != `or_user`.`uid` AND `checkEmail`.`email` = ?
            WHERE `or_user`.`uid` = ?");
        if (!$checkUser->execute([$email, $uid])) {
            return $this->jsonResponse('error', 'Ошибка при работе с БД (user check query)!');
        }
        if ($checkUser->rowCount() < 1) return $this->jsonResponse('error', 'Пользователь не найден!');

        $user = $checkUser->fetch();
        if (!empty($user['emailExist'])) {
            return $this->jsonResponse('error', 'Введённый EMail уже занят!');
        }

        if (!isset($password)) {
            $password = $user['password'];
        }

        $update = $db->prepare("UPDATE `or_user` SET `email` = ?, `password` = ?, `is_admin` = ? WHERE `uid` = ? LIMIT 1");
        if (!$update->execute([$email, $password, $is_admin, $uid])) {
            return $this->jsonResponse('error', ' Не удалось выполнить запрос к БД!');
        }

        return $this->jsonResponse('success', ' Пользователь успешно изменён!');
    }

    public function deleteUserPost($uid)
    {
        $db = DB::connect();
        $delete = $db->prepare("DELETE FROM `or_user` WHERE `uid` = ? LIMIT 1");
        if (!$delete->execute([$uid])) {
            return $this->jsonResponse('error', 'Не удалось удалить пользователя из базы данных!');
        }

        return $this->jsonResponse('success', 'Пользователь успешно удалён!');
    }

    private function jsonResponse($type, $message, $data = false)
    {
        exit(json_encode([
            'type' => $type,
            'message' => $message,
            'data' => $data
        ], JSON_UNESCAPED_UNICODE));
    }

}