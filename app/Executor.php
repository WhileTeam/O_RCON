<?php
/**
 * «O-RCON.WHILETEAM», © 2018
 * Author: KobaltMR
 */

namespace O_RCON\App;


abstract class Executor
{
    protected $server = false;

    abstract public function execute($commandID);

    /**
     * Select server with ID
     * @param int $server Server ID
     * @throws \Exception
     */
    public function selectServer($server) {
        if (!filter_var($server, FILTER_VALIDATE_INT, ['options' => ['min_range' => 0]])) throw new \Exception('Server ID must be integer!');
        $db = DB::connect();

        $checkServer = $db->query("SELECT `id`, `ip`, `port`, `pass` FROM `or_server` WHERE `id` = '{$server}' LIMIT 1");
        if ($checkServer->rowCount() < 1) {
            throw new \Exception("Сервер не найден!");
        }

        $this->server = $checkServer->fetch();
    }
}