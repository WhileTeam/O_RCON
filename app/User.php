<?php
/**
 * «O-RCON.WHILETEAM.RU», © 2018
 * Author: KobaltMR
 */

namespace O_RCON\App;


class User
{
    public static $is_auth = false;
    public $email, $uid, $is_admin = false;

    /**
     * User constructor.
     * Session parse and check if user is auth
     */
    public function __construct()
    {
        if (isset($_SESSION['auth']) && !empty($_SESSION['auth'])) {
            $authSession = json_decode($_SESSION['auth'], true);

            if (is_array($authSession) && count($authSession) === 2) {
                $email = $authSession[0];
                $passw = $authSession[1];

                $db = DB::connect();
                $checkCredentials = $db->prepare("SELECT * FROM `or_user` WHERE `email` = ? LIMIT 1");
                if (!$checkCredentials->execute([$email])) {
                    http_response_code(503);
                    exit('Не удалось выполнить запрос к БД (Auth query)');
                }
                if ($checkCredentials->rowCount() < 1) {
                    unset($_SESSION['auth']);
                } else {
                    $user = $checkCredentials->fetch();
                    if (!password_verify($passw, $user['password'])) {
                        unset($_SESSION['auth']);
                    } else {
                        self::$is_auth = true;

                        $this->uid = $user['uid'];
                        $this->email = $user['email'];
                        $this->is_admin = ($user['is_admin'] == 1) ? true : false;

                        $db->query("UPDATE `or_user` SET `last_seen` = '" . time() ."' WHERE `uid` = '{$this->uid}' LIMIT 1");
                    }
                }
            }
        }
    }

    /**
     * Logout | Auth session destructor
     * @return bool
     */
    public function logout()
    {
        if (self::$is_auth === false) return false;
        unset($_SESSION['auth']);
        return true;
    }

    /**
     * Auth session builder
     * @param string $email
     * @param string $pass
     */
    public static function setAuthSession($email, $pass)
    {
        $_SESSION['auth'] = json_encode([
            $email, $pass
        ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Encrypt password
     * @param string $newPassword Normal password
     * @return string
     */
    public static function encryptPass($password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

}