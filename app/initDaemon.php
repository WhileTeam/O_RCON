<?php
/**
 * «O-RCON.WHILETEAM», © 2018
 * Author: KobaltMR
 */

require dirname(__FILE__) . '/../app/init.php';
(new \O_RCON\App\Executors\DaemonExecutor())->initializeDaemon();