<?php
/**
 * «O-RCON.WHILETEAM», © 2018
 * Author: KobaltMR
 */

namespace O_RCON\App;


class DB
{
	private static $pdo = false;

    /**
     * Connect to database
     * @return bool|\PDO
     */
	public static function connect()
	{
        global $core;

		//if (self::$pdo === false && $core->checkInstallation()) {

			self::$pdo = new \PDO(
				'mysql:host=' . $core->getConfig()->db->host .
				';dbname=' . $core->getConfig()->db->name .
				';port=' . $core->getConfig()->db->port .
				';charset=utf8',
				$core->getConfig()->db->user,
				$core->getConfig()->db->pass,
				[
					\PDO::ATTR_ERRMODE => (\O_RCON\App\Core::isDebug()) ? \PDO::ERRMODE_EXCEPTION : \PDO::ERRMODE_SILENT,
					\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
				]
			);
		//}

		return self::$pdo;
	}

    /**
     * Close old connection and trying to connect again
     * @return bool|\PDO
     */
	public static function reConnect()
	{
		self::$pdo = false;
		return self::connect();
	}

}