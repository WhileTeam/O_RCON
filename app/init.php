<?php
/**
 * «O-RCON.WHILETEAM.RU», © 2018
 * Author: KobaltMR
 */

use O_RCON\App\User;

define('ROOT', dirname(__FILE__) . '/');

ini_set('display_errors', 'on');
error_reporting(E_ALL);

session_name('O-RCON-SID');
session_start();

require ROOT . '../vendor/autoload.php';

$core = new \O_RCON\App\Core();

if (\O_RCON\App\Core::isDebug() === false) {
    ini_set('display_errors', 'off');
}

if ($core->checkInstallation()) $core->user = new User();
