<?php
/**
 * «O-RCON.WHILETEAM», © 2018
 * Author: KobaltMR
 */

namespace O_RCON\App;

class CommandQuery
{
    /**
     * Add command to query
     *
     * @param int $server Server ID
     * @param string $command Command
     * @return array
     */
    public static function addCommand($server, $command)
    {
        if (!filter_var($server, FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]]) || !is_string($command) || empty($command)) return self::returnStatus(false, 'Недостаточно параметров!');
        if (User::$is_auth === false) return self::returnStatus(false, 'Требуется авторизация!');

        global $core;

        if (!$core->user->is_admin) {
            $cmdFounded = false;
            foreach ($core->getConfig()->rcon->blockedPrefixes->__dump() as $prefix) {
                if (strpos($command, $prefix) === 0) $cmdFounded = true;
            }

            if (in_array('*', $core->getConfig()->rcon->blockedPrefixes->__dump())) {
                if (!$cmdFounded) return self::returnStatus(false, 'Эта команда отключена!');
            } else {
                if ($cmdFounded) return self::returnStatus(false, 'Эта команда отключена!');
            }
        }

        $db = DB::connect();

        $checkServer = $db->query("SELECT `id` FROM `or_server` WHERE `id` = '{$server}' LIMIT 1");
        if ($checkServer->rowCount() < 1) {
            return self::returnStatus(false, 'Сервер не найден, попробуйте обновите страницу!');
        }

        if (!$core->user->is_admin) {
            $checkLimit = $db->prepare("SELECT `id`, `command`, `creation_time` FROM `or_query` WHERE `uid` = ? AND `server` = ? AND `creation_time` > ? AND `execution_time` IS NULL LIMIT 1");
            if (!$checkLimit->execute([$core->user->uid, $server, strtotime('-1 minute')])) {
                return self::returnStatus(false, 'Не удалось добавить команду в очередь [1]! Повторите попытку позже...');
            }

            if ($checkLimit->rowCount() > 0) {
                return self::returnStatus(false, 'Дождитесь выполнения предыдущей команды!', [
                    'command' => $checkLimit->fetch(),
                ]);
            }
        }

        $insert = $db->prepare("INSERT INTO `or_query` (`uid`, `server`, `command`, `creation_time`) VALUES (?, ?, ?, ?)");
        if (!$insert->execute([$core->user->uid, $server, $command, time()])) {
            return self::returnStatus(false, 'Не удалось добавить команду в очередь [2]! Повторите попытку позже...');
        }

        return self::returnStatus(true, 'Команда успешно добавлена', [
            'id' => $db->lastInsertId(),
        ]);
    }

    /**
     * Get execution result
     *
     * @param int $id Exec ID
     * @return array
     */
    public static function getResult($id)
    {
        global $core; // :c
        $db = DB::connect();

        $checkCommand = $db->query("SELECT `id`, `execution_time`, `result`, `server` FROM `or_query` WHERE `id` = '{$id}' LIMIT 1");
        if ($checkCommand->rowCount() < 1) {
            return self::returnStatus(false, 'Запись не найдена!');
        }

        $command = $checkCommand->fetch();
        if (empty($command['execution_time'])) {
            $executor = $core->getConfig()->rcon->executor;
            try {
                $executor = new $executor;
                $result = $executor->execute($command['id']);
            } catch (\Exception $exception) {
                return self::returnStatus(false, $exception->getMessage());
            }

            if (!is_array($result) || empty($result['result'])) {
                return self::returnStatus(true, 'Repeat', [
                    'repeat' => true
                ]);
            }
        } else {
            $result = $command['result'];
        }

        return self::returnStatus(true, 'Получен ответ!', [
            'response' => $result,
        ]);
    }

    /**
     * Return statement
     *
     * @param bool $isSuccess Is statement success
     * @param string $message Statement message
     * @param array $additionalData Additional information/data
     * @return array
     */
    private static function returnStatus($isSuccess, $message, $additionalData = [])
    {
        return [
            'success' => (is_bool($isSuccess) ? (bool)$isSuccess : false),
            'message' => $message,
            'additionalData' => $additionalData
        ];
    }
}