<?php
/**
 * «O-RCON.WHILETEAM», © 2018
 * Author: KobaltMR
 */

namespace O_RCON\App;


class BasicController
{
	protected $core, $config, $twig;

	public function __construct()
	{
		global $core;
		$this->core = $core;
		$this->config = $core->getConfig();
		$this->twig = $core->getTwig();
	}

    /**
     * Default 404 (Not Found) response
     */
	public function a404()
	{
	    http_response_code(404);
		echo $this->core->renderTpl('controllers/404.twig');
	}
}