<?php
/**
 * «O-RCON.WHILETEAM», © 2018
 * Author: KobaltMR
 */

namespace O_RCON\App\Executors;


use O_RCON\App\DB;
use O_RCON\App\Executor;
use Thedudeguy\Rcon as RCON;

class QueryExecutor extends Executor
{
    private $connection = false;

    /**
     * Connect to selected server
     * @throws \Exception
     */
    public function connect()
    {
        if ($this->server === false || !is_array($this->server)) {
            throw new \Exception('Не указан сервер!' . print_r($this->server));
        }

        try {
            $this->connection = @new Rcon($this->server['ip'], $this->server['port'], $this->server['pass'], 3);
            if (!@$this->connection->connect()) {
                throw new \Exception('Не удалось подключиться к серверу #' . $this->server['id']);
            }
        } catch (\Exception $exception) {
            throw new \Exception('Не удалось подключиться к серверу #' . $this->server['id']);
        }
    }

    /**
     * Execute a command
     * @param string $command Command
     * @return string
     * @throws \Exception
     */
    public function executeCommand($command)
    {
        if ($this->connection === false) throw new \Exception('RCON not connected yet!');
        $this->connection->sendCommand($command);
        return trim($this->connection->getResponse());
    }

    /**
     * Execute by command by query id
     * @param int $commandID row ID
     * @return string
     * @throws \Exception
     */
    public function execute($commandID)
    {
        if (!filter_var($commandID, FILTER_VALIDATE_INT, ['options' > ["min_range" => 0]])) throw new \Exception('Невалидный ID!');

        $db = DB::connect();
        $command = $db->query("SELECT `command`, `execution_time`, `server` FROM `or_query` WHERE `id` = '{$commandID}' LIMIT 1");

        if ($command->rowCount() < 1) throw new \Exception('Запрос не найден!');
        $command = $command->fetch();
        if (!empty($command['execution_time'])) throw new \Exception('Команда уже выполнена!');

        if ($this->connection === false) {
            $this->selectServer($command['server']);
            $this->connect();
        }

        $response = $this->executeCommand($command['command']);

        $update = $db->prepare("UPDATE `or_query` SET `execution_time` = ?, `result` = ? WHERE `id` = ? LIMIT 1");
        if (!$update->execute([time(), $response, $commandID])) {
            throw new \Exception('Не удалось выполнить запрос к БД!');
        }

        return $response;
    }
}