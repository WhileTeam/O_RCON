<?php
/**
 * «O-RCON.WHILETEAM», © 2018
 * Author: KobaltMR
 */

namespace O_RCON\App\Executors;

use O_RCON\App\Core;
use O_RCON\App\DB;
use O_RCON\App\Executor;
use Thedudeguy\Rcon;

class DaemonExecutor extends Executor
{
    private $fileLogging = false;

    /**
     * Daemons initialize
     * @throws \Exception
     */
    public function initializeDaemon()
    {
        if (Core::isDebug() === true) $this->fileLogging = true;

        $mainDaemon = \pcntl_fork();
        if ($mainDaemon == -1) {
            throw new \Exception('Couldn`t initialize main daemon!');
        } else if ($mainDaemon) die;

        \posix_setsid();

        $db = DB::connect();
        $servers = $db->query("SELECT `id`, `ip`, `port`, `pass`, `pid` FROM `or_server` ORDER BY `id`");
        foreach ($servers as $server) {
            if (!empty($server['pid']) && \posix_getpgid($server['pid']) == true) {
                $this->log('Sever #' . $server['id'] . ' — already initialized, skipping...');
                continue;
            }

            $serverDaemon = \pcntl_fork();
            if ($serverDaemon == -1) {
                $this->log('Sever #' . $server['id'] . ' — couldn`t create daemon!');
            } else if ($serverDaemon) {
                try {
                    // Firstly update pid in DB, to exclude daemon duplication
                    $db = DB::reConnect();
                    $db->query("UPDATE `or_server` SET `pid` = '" . \getmypid() . "' WHERE `id` = '{$server['id']}' LIMIT 1");

                    $rcon = @new Rcon($server['ip'], $server['port'], $server['pass'], 3);
                    if (!@$rcon->connect()) {
                        $this->log('Sever #' . $server['id'] . ' — couldn`t connect to RCON!');
                        die;
                    }
                    $this->log('Sever #' . $server['id'] . ' — RCON successful connected!');

                    $alive = true;
                    \pcntl_signal(SIGTERM, function () {
                        global $alive;
                        $alive = false;
                    });

                    $lastPidCheck = time();

                    while ($alive) {
                        $query = $db->query("SELECT `id`, `command` FROM `or_query` WHERE `server` = '{$server['id']}' AND `execution_time` IS NULL LIMIT 5");
                        foreach ($query as $command) {
                            try {
                                $this->log('Executing: ' . $command['command']);

                                $rcon->sendCommand($command['command']);
                                $response = $rcon->getResponse();

                                $this->log('Response: ' . $response);

                                $update = $db->prepare("UPDATE `or_query` SET `result` = ?, `execution_time` = ? WHERE `id` = ? LIMIT 1");
                                if (!$update->execute([$response, time(), $command['id']])) {
                                    $this->log('Sever #' . $server['id'] . ', query # ' . $command['id'] . ' — couldn`t update row!');
                                }
                            } catch (\Exception $ex) {
                                $this->log('Sever #' . $server['id'] . ' — ' . $ex->getMessage());
                                $this->log('Sever #' . $server['id'] . ' — fatal exception! Turning off daemon');
                                $alive = false;
                            }
                        }

                        if (time() - $lastPidCheck > 150) {
                            $checkPid = $db->query("SELECT `pid` FROM `or_server` WHERE `id` = '{$server['id']}' LIMIT 1");
                            if ($checkPid->rowCount() < 1) die;
                            $checkPid = $checkPid->fetch()['pid'];
                            if ($checkPid != \getmypid()) {
                                $this->log('Sever #' . $server['id'] . ' — Pid not equals!');
                                die;
                            }
                            $lastPidCheck = time();
                        }
                        sleep(1.5);
                    }
                } catch (\Exception $ex) {
                    $this->log('Sever #' . $server['id'] . ' — ' . $ex->getMessage());
                }
            }
        }
    }

    /**
     * Logger
     * When debug is true, log also writing in file (path_to_script_dir/daemon.log)
     * @param $message
     */
    private function log($message)
    {
        $message = '[' . date('H:i:s') . '] ' . $message . PHP_EOL;

        if ($this->fileLogging === true) {
            @file_put_contents(ROOT . '../daemon.log', $message, FILE_APPEND);
        }
        echo $message;
    }

    /**
     * Bck run daemon if may
     * @param int $commandID not needed
     * @return bool|void
     * @throws \Exception
     */
    public function execute($commandID)
    {
        if (!function_exists('pcntl_fork')) {
            // apache?
            return false;
        }

        (new \O_RCON\App\Executors\DaemonExecutor())->initializeDaemon();
    }
}
