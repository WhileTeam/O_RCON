<?php
/**
 * «O-RCON.WHILETEAM.RU», © 2018
 * Author: KobaltMR
 */

namespace O_RCON\App;
use \KobaltMR\Config as Config;
use \NickStuer\EasyRouter\Route as Route;

class Core
{
	private $twig, $config, $controller;
	private static $debug = true;
	public $user;

	/**
	 * Core constructor.
	 */
	public function __construct()
	{
		try {
			$this->config = (new Config())->__fromFile(ROOT . '../config.php');
		} catch (\Exception $e) {
			die('Не удалось загрузить конфигурационный файл!');
		}

		self::$debug = (bool) $this->getConfig()->debug;

		self::setTwig();
	}

	public static function isDebug()
    {
        return (bool) self::$debug;
    }

    public static function checkArrayFields($fields, $array)
    {
        foreach ($fields as $field) {
            if (empty($array[$field])) return false;
        }
        return true;
    }

	/**
	 * Initialize Twig
	 */
	private function setTwig()
	{
		$this->twig = new \Twig_Environment(
			new \Twig_Loader_Filesystem(ROOT . '../template/'),
			[
				'cache' => (($this->config->global->templateCache === true) ? ROOT . '../.cache' : false),
			]
		);
	}

	/**
	 * Get Twig instance
	 *
	 * @return \Twig_Environment
	 */
	public function getTwig()
	{
		return $this->twig;
	}

	/**
	 * Render template
	 *
	 * @param string $file Path to template (in `template` dir)
	 * @param mixed $additionalData Addition data for template
	 * @return string
	 */
	public function renderTpl($file, $additionalData = [])
	{
        try {
            return $this->getTwig()->render($file, [
                'core' => $this,
                'config' => $this->config,
                'data' => $additionalData,
            ]);
        } catch (\Twig_Error_Loader $e) {
            exit('Twig fatal err: ' . $e);
        } catch (\Twig_Error_Runtime $e) {
            exit('Twig runtime err: ' . $e);
        } catch (\Twig_Error_Syntax $e) {
            exit('Twig syntax err: ' . $e);
        }
    }

	/**
	 * Get global config
	 *
	 * @return Config
	 */
	public function getConfig()
	{
		return $this->config;
	}

	/**
	 * Router base (fck)
	 *
	 * @return string
	 */
	public function setupController()
	{
		$routeManager = new \NickStuer\EasyRouter\RouteManager();
		$router = new \NickStuer\EasyRouter\Dispatcher($routeManager);

		// Routes
		foreach ($this->config->routes->__dump() as $route) {
			$routeManager->addRoute(new Route($route['method'], $route['uri'], $route['controller']));
		}

		try {
			$router->dispatch();

			$controllerName = $router->getMatchedRoute()['controller'];
			$methodName = $router->getMatchedRoute()['method'];
			$methodArgs = $router->getMatchedRoute()['variables'];

			if (!\class_exists('\\O_RCON\\Controller\\' . $controllerName)) throw new \Exception('Controller not found!');
		} catch (\Exception $e) {
			$controllerName = "DashboardController";
			$methodName = "a404";
			$methodArgs = [];
		}

		$controllerName = '\\O_RCON\\Controller\\' . $controllerName;
		$this->controller = new $controllerName();

		\ob_start();
		if (!\is_array($methodArgs)) $methodArgs = [];
		echo \call_user_func_array([$this->controller, $methodName], $methodArgs);
		return \ob_get_clean();
	}

    /**
     * Check script installation state
     *
     * @return bool
     */
	public function checkInstallation()
    {
        return ($this->config->installed == true);
    }
}